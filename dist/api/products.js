"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const async_1 = require("async");
const bodyParser = require("body-parser");
const lodash_1 = require("lodash");
const is_user_1 = require("../middleware/is-user");
function handleProducts(server, app) {
    server.delete('/api/products/:id', is_user_1.isUser, (req, res) => {
        app.deleteProduct(req.user._id, this.params.id, (err, rowCount) => {
            if (err)
                return res.status(500).send(JSON.stringify(err.message));
            return res.send(JSON.stringify(rowCount));
        });
    });
    server.get('/api/products', is_user_1.isUser, (req, res) => {
        app.getProducts(req.user._id, (err, products) => {
            if (err)
                return res.status(500).send(JSON.stringify(err.message));
            return res.send(JSON.stringify(products));
        });
    });
    server.get('/api/products/sold', is_user_1.isUser, (req, res) => {
        app.getProducts(req.user._id, { sold: true }, (err, products) => {
            if (err)
                return res.status(500).send(JSON.stringify(err.message));
            return res.send(JSON.stringify(products));
        });
    });
    server.get('/api/products/purchased', is_user_1.isUser, (req, res) => {
        app.getProducts(req.user._id, { produced: false }, (err, products) => {
            if (err)
                return res.status(500).send(JSON.stringify(err.message));
            return res.send(JSON.stringify(products));
        });
    });
    server.get('/api/products/:id', is_user_1.isUser, (req, res) => {
        app.getProducts(req.user._id, [parseInt(req.params.id)], (err, products) => {
            if (err)
                return res.status(500).send(JSON.stringify(err.message));
            return res.send(JSON.stringify(products[0]));
        });
    });
    server.get('/api/products/:id/monthly-sales', is_user_1.isUser, (req, res) => {
        app.getMonthlySales(req.user._id, parseInt(req.params.id), (err, sales) => {
            if (err)
                return res.status(500).send(JSON.stringify(err.message));
            return res.send(JSON.stringify(sales));
        });
    });
    server.post('/api/products', is_user_1.isUser, bodyParser.json(), (req, res) => {
        let values = lodash_1.pick(req.body, ['code', 'name', 'content', 'uom', 'price', 'cost_fixed', 'sold', 'produced']);
        async_1.waterfall([
            (cb) => app.addProduct(req.user._id, values, cb),
            (id, cb) => {
                if (req.body.recipe) {
                    async_1.each(req.body.recipe, (r, _cb) => {
                        app.addRecipesRecord({ parent_id: id, child_id: r.child_id, coef: r.coef }, _cb);
                    }, cb);
                }
                else
                    return cb(null);
            }
        ], (err) => {
            if (err)
                return res.status(500).send(JSON.stringify(err.message));
            return res.send(JSON.stringify(true));
        });
    });
    server.put('/api/products/:id', is_user_1.isUser, bodyParser.json(), (req, res) => {
        let values = lodash_1.pick(req.body, ['code', 'name', 'content', 'uom', 'price', 'cost_fixed', 'sold', 'produced']);
        async_1.waterfall([
            (cb) => app.updateProduct(req.user._id, req.params.id, values, cb),
            (rowCount, cb) => {
                if (req.body.recipe) {
                    app.deleteRecipe(req.params.id, cb);
                }
                else
                    return cb(null, 0);
            },
            (rowCount, cb) => {
                if (req.body.recipe) {
                    async_1.each(req.body.recipe, (r, _cb) => {
                        app.addRecipesRecord({ parent_id: req.params.id, child_id: r.child_id, coef: r.coef }, _cb);
                    }, cb);
                }
                else
                    return cb(null);
            }
        ], (err) => {
            if (err)
                return res.status(500).send(JSON.stringify(err.message));
            return res.send(JSON.stringify(true));
        });
    });
}
exports.handleProducts = handleProducts;
//# sourceMappingURL=products.js.map
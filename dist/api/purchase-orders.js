"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const async_1 = require("async");
const bodyParser = require("body-parser");
const lodash_1 = require("lodash");
const is_user_1 = require("../middleware/is-user");
function handlePurchaseOrders(server, app) {
    server.get('/api/purchase-orders', is_user_1.isUser, (req, res) => {
        app.getPurchaseOrders(req.user._id, (err, orders) => {
            if (err)
                return res.status(500).send(JSON.stringify(err.message));
            return res.send(JSON.stringify(orders));
        });
    });
    server.get('/api/purchase-orders/:id', is_user_1.isUser, (req, res) => {
        app.getPurchaseOrders(req.user._id, [parseInt(req.params.id)], (err, orders) => {
            if (err)
                return res.status(500).send(JSON.stringify(err.message));
            return res.send(JSON.stringify(orders[0]));
        });
    });
    server.get('/api/purchase-orders/:id/lines', is_user_1.isUser, (req, res) => {
        app.getPurchaseOrderLines(parseInt(req.params.id), (err, lines) => {
            if (err)
                return res.status(500).send(JSON.stringify(err.message));
            return res.send(JSON.stringify(lines));
        });
    });
    server.post('/api/purchase-orders', is_user_1.isUser, bodyParser.json(), (req, res) => {
        let values = lodash_1.pick(req.body, ['supplier', 'number', 'date', 'total']);
        let id;
        async_1.waterfall([
            (cb) => app.addPurchaseOrder(req.user._id, values, cb),
            (_id, cb) => {
                id = _id;
                if (req.body.order_lines) {
                    async_1.each(req.body.order_lines, (r, _cb) => {
                        app.addPurchaseOrderLine(_id, r, _cb);
                    }, cb);
                }
                else
                    return cb(null);
            }
        ], (err) => {
            if (err)
                return res.status(500).send(JSON.stringify(err.message));
            return res.send(JSON.stringify(id));
        });
    });
    server.put('/api/purchase-orders/:id', is_user_1.isUser, bodyParser.json(), (req, res) => {
        let values = lodash_1.pick(req.body, ['supplier', 'number', 'date', 'total']);
        async_1.waterfall([
            (cb) => app.updatePurchaseOrder(req.user._id, req.params.id, values, cb),
            (rowCount, cb) => app.deletePurchaseOrderLines(req.params.id, cb),
            (rowCount, cb) => {
                if (req.body.order_lines) {
                    async_1.each(req.body.order_lines, (r, _cb) => {
                        app.addPurchaseOrderLine(req.params.id, r, _cb);
                    }, cb);
                }
                else
                    return cb(null);
            }
        ], (err) => {
            if (err)
                return res.status(500).send(JSON.stringify(err.message));
            return res.send(JSON.stringify(true));
        });
    });
}
exports.handlePurchaseOrders = handlePurchaseOrders;
//# sourceMappingURL=purchase-orders.js.map
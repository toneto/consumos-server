"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const is_user_1 = require("../middleware/is-user");
function handleRecipes(server, app) {
    server.get('/api/recipes/:productId', is_user_1.isUser, (req, res) => {
        app.getRecipe(parseInt(req.params.productId), (err, products) => {
            if (err)
                return res.status(500).send(JSON.stringify(err.message));
            return res.send(JSON.stringify(products));
        });
    });
    server.delete('/api/recipes/:productId', is_user_1.isUser, (req, res) => {
        app.deleteRecipe(parseInt(req.params.productId), (err, rowCount) => {
            if (err)
                return res.status(500).send(JSON.stringify(err.message));
            return res.send(JSON.stringify(rowCount));
        });
    });
}
exports.handleRecipes = handleRecipes;
//# sourceMappingURL=recipes.js.map
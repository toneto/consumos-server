"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const async_1 = require("async");
const bodyParser = require("body-parser");
const lodash_1 = require("lodash");
const is_user_1 = require("../middleware/is-user");
function handleSalesOrders(server, app) {
    server.get('/api/sales-orders', is_user_1.isUser, (req, res) => {
        app.getSalesOrders(req.user._id, (err, orders) => {
            if (err)
                return res.status(500).send(JSON.stringify(err.message));
            return res.send(JSON.stringify(orders));
        });
    });
    server.get('/api/sales-orders/:id', is_user_1.isUser, (req, res) => {
        app.getSalesOrders(req.user._id, [parseInt(req.params.id)], (err, orders) => {
            if (err)
                return res.status(500).send(JSON.stringify(err.message));
            return res.send(JSON.stringify(orders[0]));
        });
    });
    server.get('/api/sales-orders/:id/lines', is_user_1.isUser, (req, res) => {
        app.getSalesOrderLines(parseInt(req.params.id), (err, lines) => {
            if (err)
                return res.status(500).send(JSON.stringify(err.message));
            return res.send(JSON.stringify(lines));
        });
    });
    server.post('/api/sales-orders', is_user_1.isUser, bodyParser.json(), (req, res) => {
        let values = lodash_1.pick(req.body, ['client', 'date']);
        async_1.waterfall([
            (cb) => app.addSalesOrder(req.user._id, values, cb),
            (id, cb) => {
                if (req.body.order_lines) {
                    async_1.each(req.body.order_lines, (r, _cb) => {
                        app.addSalesOrderLine(id, r, _cb);
                    }, cb);
                }
                else
                    return cb(null);
            }
        ], (err) => {
            if (err)
                return res.status(500).send(JSON.stringify(err.message));
            return res.send(JSON.stringify(true));
        });
    });
    server.put('/api/sales-orders/:id', is_user_1.isUser, bodyParser.json(), (req, res) => {
        let values = lodash_1.pick(req.body, ['client', 'date']);
        async_1.waterfall([
            (cb) => app.updateSalesOrder(req.user._id, req.params.id, values, cb),
            (rowCount, cb) => app.deleteSalesOrderLines(req.params.id, cb),
            (rowCount, cb) => {
                if (req.body.order_lines) {
                    async_1.each(req.body.order_lines, (r, _cb) => {
                        app.addSalesOrderLine(req.params.id, r, _cb);
                    }, cb);
                }
                else
                    return cb(null);
            }
        ], (err) => {
            if (err)
                return res.status(500).send(JSON.stringify(err.message));
            return res.send(JSON.stringify(true));
        });
    });
}
exports.handleSalesOrders = handleSalesOrders;
//# sourceMappingURL=sales-orders.js.map
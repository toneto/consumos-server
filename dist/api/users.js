"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const bodyParser = require("body-parser");
const lodash_1 = require("lodash");
const md5 = require("md5");
const is_user_1 = require("../middleware/is-user");
function handleUsers(server, app) {
    server.put('/api/users/update-password', is_user_1.isUser, bodyParser.json(), (req, res) => {
        let values = lodash_1.pick(req.body, ['old', 'new']);
        if (md5(values.old) != req.user.password) {
            return res.status(400).send('Contraseña incorrecta!');
        }
        app.updateUser(req.user._id, { password: values.new }, (err) => {
            if (err)
                return res.status(500).send(JSON.stringify(err.message));
            return res.send(JSON.stringify(true));
        });
    });
}
exports.handleUsers = handleUsers;
//# sourceMappingURL=users.js.map
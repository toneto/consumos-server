"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const bodyParser = require("body-parser");
const session = require("express-session");
const passport = require("passport");
const passport_local_1 = require("passport-local");
function handleAuth(server, app) {
    passport.use(new passport_local_1.Strategy((username, password, done) => {
        app.authenticate(username, password, (err, user) => {
            if (err)
                return done(err);
            if (user)
                return done(null, user);
            return done(null, false, { message: 'Usuario o contraseña incorrectos!' });
        });
    }));
    passport.serializeUser((user, done) => {
        done(null, JSON.stringify(user));
    });
    passport.deserializeUser((json, done) => {
        return done(null, JSON.parse(json));
    });
    server.use(session({ secret: "emprendedores", resave: false, saveUninitialized: false }));
    server.use(bodyParser.urlencoded({ extended: false }));
    server.use(passport.initialize());
    server.use(passport.session());
    server.post('/auth/login', passport.authenticate('local'), (req, res) => {
        res.send(JSON.stringify(req.user));
    });
    server.get('/auth/logout', (req, res) => {
        req.logOut();
        return res.send(JSON.stringify(true));
    });
    server.get('/auth/user-data', (req, res) => {
        return res.send(JSON.stringify(req.user));
    });
}
exports.handleAuth = handleAuth;
//# sourceMappingURL=auth.js.map
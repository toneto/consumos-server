"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const server_1 = require("./server");
const PORT = 8080;
server_1.createServer().listen(PORT, () => console.log(`Server listening on port: ${PORT}`));
//# sourceMappingURL=index.js.map
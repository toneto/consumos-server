"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function isUser(req, res, next) {
    if (!req.user)
        return res.status(401).send('Sin acceso!');
    return next();
}
exports.isUser = isUser;
//# sourceMappingURL=is-user.js.map
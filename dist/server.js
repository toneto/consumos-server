"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const consumos_1 = require("consumos");
const express = require("express");
const products_1 = require("./api/products");
const purchase_orders_1 = require("./api/purchase-orders");
const recipes_1 = require("./api/recipes");
const sales_orders_1 = require("./api/sales-orders");
const users_1 = require("./api/users");
const auth_1 = require("./auth");
// const GOOGLE_CLIENT_KEY = '126884428237-qh5vflad3oljatu6dtr6ra3pcen3ho54.apps.googleusercontent.com';
// const GOOGLE_CLIENT_SECRET = '_RUWcniB1J_VFraUn_DUI12o';
function createServer() {
    let server = express();
    let app = new consumos_1.App({});
    // server.use(accessControlAllowOrigin);
    auth_1.handleAuth(server, app);
    products_1.handleProducts(server, app);
    purchase_orders_1.handlePurchaseOrders(server, app);
    recipes_1.handleRecipes(server, app);
    sales_orders_1.handleSalesOrders(server, app);
    users_1.handleUsers(server, app);
    return server;
}
exports.createServer = createServer;
//# sourceMappingURL=server.js.map
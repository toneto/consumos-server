import { each, waterfall } from 'async';
import * as bodyParser from 'body-parser';
import { App, RecipesRecord } from 'consumos';
import * as express from 'express';
import { pick } from 'lodash';

import { isUser } from '../middleware/is-user';

export function handleProducts(server: express.Express, app: App) {
  server.delete('/api/products/:id',
    isUser,
    (req, res) => {
      app.deleteProduct(req.user._id, this.params.id, (err, rowCount) => {
        if (err) return res.status(500).send(JSON.stringify(err.message));

        return res.send(JSON.stringify(rowCount));
      })
    }
  );

  server.get('/api/products',
    isUser,
    (req, res) => {
      app.getProducts(req.user._id, (err, products) => {
        if (err) return res.status(500).send(JSON.stringify(err.message));

        return res.send(JSON.stringify(products));
      })
    }
  );

  server.get('/api/products/sold',
    isUser,
    (req, res) => {
      app.getProducts(req.user._id, {sold: true}, (err, products) => {
        if (err) return res.status(500).send(JSON.stringify(err.message));

        return res.send(JSON.stringify(products));
      })
    }
  );

  server.get('/api/products/purchased',
    isUser,
    (req, res) => {
      app.getProducts(req.user._id, {produced: false}, (err, products) => {
        if (err) return res.status(500).send(JSON.stringify(err.message));

        return res.send(JSON.stringify(products));
      });
    }
  );

  server.get('/api/products/:id',
    isUser,
    (req, res) => {
      app.getProducts(req.user._id, [parseInt(req.params.id)], (err, products) => {
        if (err) return res.status(500).send(JSON.stringify(err.message));

        return res.send(JSON.stringify(products[0]));
      })
    }
  );

  server.get('/api/products/:id/monthly-sales',
    isUser,
    (req, res) => {
      app.getMonthlySales(req.user._id, parseInt(req.params.id), (err, sales) => {
        if (err) return res.status(500).send(JSON.stringify(err.message));

        return res.send(JSON.stringify(sales));
      })
    }
  );

  server.post('/api/products',
    isUser,
    bodyParser.json(),
    (req, res) => {
      let values = pick(req.body, ['code', 'name', 'content', 'uom', 'price', 'cost_fixed', 'sold', 'produced']);

      waterfall([
        (cb: (err: Error, id: number) => void) => app.addProduct(req.user._id, values, cb),
        (id: number, cb: (err: Error) => void) => {
          if (req.body.recipe) {
            each(req.body.recipe as RecipesRecord[], (r, _cb) => {
              app.addRecipesRecord({parent_id: id, child_id: r.child_id, coef: r.coef}, _cb);
            }, cb)
          } else return cb(null);
        }
      ], (err: Error) => {
        if (err) return res.status(500).send(JSON.stringify(err.message));

        return res.send(JSON.stringify(true));
      })
    }
  )

  server.put('/api/products/:id',
    isUser,
    bodyParser.json(),
    (req, res) => {
      let values = pick(req.body, ['code', 'name', 'content', 'uom', 'price', 'cost_fixed', 'sold', 'produced']);

      waterfall([
        (cb: (err: Error, rowCount: number) => void) => app.updateProduct(req.user._id, req.params.id, values, cb),
        (rowCount: number, cb: (err: Error, rowCount: number) => void) => {
          if (req.body.recipe) {
            app.deleteRecipe(req.params.id, cb)
          }
          else return cb(null, 0);
        },
        (rowCount: number, cb: (err: Error) => void) => {
          if (req.body.recipe) {
            each(req.body.recipe, (r: RecipesRecord, _cb) => {
              app.addRecipesRecord({ parent_id: req.params.id, child_id: r.child_id, coef: r.coef }, _cb);
            }, cb)
          } else return cb(null);
        }
      ], (err: Error) => {
        if (err) return res.status(500).send(JSON.stringify(err.message));

        return res.send(JSON.stringify(true));
      })
    }
  );
}

import { each, waterfall } from 'async';
import * as bodyParser from 'body-parser';
import * as express from 'express';
import { App, PurchaseOrdersLineRecord } from 'consumos';
import { pick } from 'lodash';

import { isUser } from '../middleware/is-user';


export function handlePurchaseOrders(server: express.Express, app: App) {
  server.get('/api/purchase-orders',
    isUser,
    (req, res) => {
      app.getPurchaseOrders(req.user._id, (err, orders) => {
        if (err) return res.status(500).send(JSON.stringify(err.message));

        return res.send(JSON.stringify(orders));
      })
    }
  );

  server.get('/api/purchase-orders/:id',
    isUser,
    (req, res) => {
      app.getPurchaseOrders(req.user._id, [parseInt(req.params.id)], (err, orders) => {
        if (err) return res.status(500).send(JSON.stringify(err.message));

        return res.send(JSON.stringify(orders[0]));
      })
    }
  );

  server.get('/api/purchase-orders/:id/lines',
    isUser,
    (req, res) => {
      app.getPurchaseOrderLines(parseInt(req.params.id), (err, lines) => {
        if (err) return res.status(500).send(JSON.stringify(err.message));

        return res.send(JSON.stringify(lines));
      })
    }
  );

  server.post('/api/purchase-orders',
    isUser,
    bodyParser.json(),
    (req, res) => {
      let values = pick(req.body, ['supplier', 'number', 'date', 'total']);
      let id: number;

      waterfall([
        (cb: (err: Error, id: number) => void) => app.addPurchaseOrder(req.user._id, values, cb),
        (_id: number, cb: (err: Error) => void) => {
          id = _id;
          if (req.body.order_lines) {
            each(req.body.order_lines as PurchaseOrdersLineRecord[], (r, _cb) => {
              app.addPurchaseOrderLine(_id, r, _cb);
            }, cb);
          } else return cb(null);
        }
      ], (err: Error) => {
        if (err) return res.status(500).send(JSON.stringify(err.message));

        return res.send(JSON.stringify(id));
      })
    }
  )

  server.put('/api/purchase-orders/:id',
    isUser,
    bodyParser.json(),
    (req, res) => {
      let values = pick(req.body, ['supplier', 'number', 'date', 'total']);

      waterfall([
        (cb: (err: Error, rowCount: number) => void) => app.updatePurchaseOrder(req.user._id, req.params.id, values, cb),
        (rowCount: number, cb: (err: Error, rowCount: number) => void) => app.deletePurchaseOrderLines(req.params.id, cb),
        (rowCount: number, cb: (err: Error) => void) => {
          if (req.body.order_lines) {
            each(req.body.order_lines, (r: PurchaseOrdersLineRecord, _cb) => {
              app.addPurchaseOrderLine(req.params.id, r, _cb);
            }, cb);
          } else return cb(null);
        }
      ], (err: Error) => {
        if (err) return res.status(500).send(JSON.stringify(err.message));

        return res.send(JSON.stringify(true));
      })
    }
  );


}

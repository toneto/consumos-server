import * as bodyParser from 'body-parser';
import { App } from 'consumos';
import * as express from 'express';
import { pick } from 'lodash';

import { isUser } from '../middleware/is-user';

export function handleRecipes(server: express.Express, app: App) {
  server.get('/api/recipes/:productId',
    isUser,
    (req, res) => {
      app.getRecipe(parseInt(req.params.productId), (err, products) => {
        if (err) return res.status(500).send(JSON.stringify(err.message));

        return res.send(JSON.stringify(products));
      })
    }
  );

  server.delete('/api/recipes/:productId',
    isUser,
    (req, res) => {
      app.deleteRecipe(parseInt(req.params.productId), (err, rowCount) => {
        if (err) return res.status(500).send(JSON.stringify(err.message));

        return res.send(JSON.stringify(rowCount));
      });
    }
  )
}

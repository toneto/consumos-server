import { each, waterfall } from 'async';
import * as bodyParser from 'body-parser';
import * as express from 'express';
import { App, SalesOrdersLineRecord } from 'consumos';
import { pick } from 'lodash';

import { isUser } from '../middleware/is-user';


export function handleSalesOrders(server: express.Express, app: App) {
  server.get('/api/sales-orders',
    isUser,
    (req, res) => {
      app.getSalesOrders(req.user._id, (err, orders) => {
        if (err) return res.status(500).send(JSON.stringify(err.message));

        return res.send(JSON.stringify(orders));
      })
    }
  );

  server.get('/api/sales-orders/:id',
    isUser,
    (req, res) => {
      app.getSalesOrders(req.user._id, [parseInt(req.params.id)], (err, orders) => {
        if (err) return res.status(500).send(JSON.stringify(err.message));

        return res.send(JSON.stringify(orders[0]));
      })
    }
  );

  server.get('/api/sales-orders/:id/lines',
    isUser,
    (req, res) => {
      app.getSalesOrderLines(parseInt(req.params.id), (err, lines) => {
        if (err) return res.status(500).send(JSON.stringify(err.message));

        return res.send(JSON.stringify(lines));
      })
    }
  );

  server.post('/api/sales-orders',
    isUser,
    bodyParser.json(),
    (req, res) => {
      let values = pick(req.body, ['client', 'date']);

      waterfall([
        (cb: (err: Error, id: number) => void) => app.addSalesOrder(req.user._id, values, cb),
        (id: number, cb: (err: Error) => void) => {
          if (req.body.order_lines) {
            each(req.body.order_lines as SalesOrdersLineRecord[], (r, _cb) => {
              app.addSalesOrderLine(id, r, _cb);
            }, cb);
          } else return cb(null);
        }
      ], (err: Error) => {
        if (err) return res.status(500).send(JSON.stringify(err.message));

        return res.send(JSON.stringify(true));
      })
    }
  )

  server.put('/api/sales-orders/:id',
    isUser,
    bodyParser.json(),
    (req, res) => {
      let values = pick(req.body, ['client', 'date']);

      waterfall([
        (cb: (err: Error, rowCount: number) => void) => app.updateSalesOrder(req.user._id, req.params.id, values, cb),
        (rowCount: number, cb: (err: Error, rowCount: number) => void) => app.deleteSalesOrderLines(req.params.id, cb),
        (rowCount: number, cb: (err: Error) => void) => {
          if (req.body.order_lines) {
            each(req.body.order_lines, (r: SalesOrdersLineRecord, _cb) => {
              app.addSalesOrderLine(req.params.id, r, _cb);
            }, cb);
          } else return cb(null);
        }
      ], (err: Error) => {
        if (err) return res.status(500).send(JSON.stringify(err.message));

        return res.send(JSON.stringify(true));
      })
    }
  );

}

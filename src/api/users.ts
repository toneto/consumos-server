
import { each, waterfall } from 'async';
import * as bodyParser from 'body-parser';
import { App, RecipesRecord } from 'consumos';
import * as express from 'express';
import { pick } from 'lodash';
import * as md5 from 'md5';

import { isUser } from '../middleware/is-user';

export function handleUsers(server: express.Express, app: App) {

  server.put('/api/users/update-password',
    isUser,
    bodyParser.json(),
    (req, res) => {
      let values = pick(req.body, ['old', 'new']);

      if (md5(values.old) != req.user.password) {
        return res.status(400).send('Contraseña incorrecta!');
      }

      app.updateUser(req.user._id, {password: values.new}, (err) => {
        if (err) return res.status(500).send(JSON.stringify(err.message));

        return res.send(JSON.stringify(true));
      })
    }
  );
}

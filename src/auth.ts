import * as bodyParser from 'body-parser';
import { App } from 'consumos';
import * as express from 'express';
import * as session from 'express-session';
import * as passport from 'passport';
import { Strategy as LocalStrategy } from 'passport-local';

interface User {
  id: number;
  name: string;
}

export function handleAuth(server: express.Express, app: App) {

  passport.use(new LocalStrategy(
    (username, password, done) => {
      app.authenticate(username, password, (err, user) => {
        if (err) return done(err);

        if (user) return done(null, user);

        return done(null, false, {message: 'Usuario o contraseña incorrectos!'});
      })
    }
  ));

  passport.serializeUser((user: User, done) => {
    done(null, JSON.stringify(user));
  });

  passport.deserializeUser((json: string, done) => {
    return done(null, JSON.parse(json) as User);
  });

  server.use(session({secret: "emprendedores", resave: false, saveUninitialized: false}));
  server.use(bodyParser.urlencoded({ extended: false }));
  server.use(passport.initialize());
  server.use(passport.session());

  server.post('/auth/login',
    passport.authenticate('local'),
    (req, res) => {
      res.send(JSON.stringify(req.user));
    }
  );

  server.get('/auth/logout',
    (req, res) => {
      req.logOut();
      return res.send(JSON.stringify(true));
    }
  );

  server.get('/auth/user-data',
    (req, res) => {
      return res.send(JSON.stringify(req.user));
    }
  )

}

import { createServer } from './server';

const PORT = 8080;

createServer().listen(PORT, () => console.log(`Server listening on port: ${PORT}`));

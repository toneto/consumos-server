import * as express from 'express';

export function isUser(req: express.Request, res: express.Response, next: express.NextFunction) {
  if (!req.user) return res.status(401).send('Sin acceso!');

  return next();
}

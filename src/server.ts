import { App } from 'consumos';
import * as express from 'express';
import * as session from 'express-session';
import * as passport from 'passport';

import { handleProducts } from './api/products';
import { handlePurchaseOrders } from './api/purchase-orders';
import { handleRecipes } from './api/recipes';
import { handleSalesOrders } from './api/sales-orders';
import { handleUsers } from './api/users';
import { handleAuth } from './auth';
// import { accessControlAllowOrigin } from './middleware/access-control-allow-origin';
import { isUser } from './middleware/is-user';

// const GOOGLE_CLIENT_KEY = '126884428237-qh5vflad3oljatu6dtr6ra3pcen3ho54.apps.googleusercontent.com';
// const GOOGLE_CLIENT_SECRET = '_RUWcniB1J_VFraUn_DUI12o';


export function createServer() {
  let server = express();
  let app = new App({});

  // server.use(accessControlAllowOrigin);
  handleAuth(server, app);
  handleProducts(server, app);
  handlePurchaseOrders(server, app);
  handleRecipes(server, app);
  handleSalesOrders(server, app);
  handleUsers(server, app);

  return server;
}
